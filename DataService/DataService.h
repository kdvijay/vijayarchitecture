//
//  DataService.h
//  DataService
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 4/30/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for DataService.
FOUNDATION_EXPORT double DataServiceVersionNumber;

//! Project version string for DataService.
FOUNDATION_EXPORT const unsigned char DataServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <DataService/PublicHeader.h>


