//
//  ConfigurationService.h
//  ConfigurationService
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 4/30/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for ConfigurationService.
FOUNDATION_EXPORT double ConfigurationServiceVersionNumber;

//! Project version string for ConfigurationService.
FOUNDATION_EXPORT const unsigned char ConfigurationServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ConfigurationService/PublicHeader.h>


