//
//  ActionHandler.swift
//  UIService
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 4/30/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

import Foundation
import APIService

public class ActionHandler: ActionHandlerProtocol {
    
    public static func performAction() -> String {
        return NetworkManager.executeRequest()
    }
}
