//
//  UIService.h
//  UIService
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 4/30/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for UIService.
FOUNDATION_EXPORT double UIServiceVersionNumber;

//! Project version string for UIService.
FOUNDATION_EXPORT const unsigned char UIServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <UIService/PublicHeader.h>


