//
//  NetworkManager.swift
//  APIService
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 4/30/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

import ConfigurationService

public class NetworkManager: Network {
    public static func executeRequest() -> String
    {
        return ConfigurationManager.getConfiguration()
    }
    
    
}
