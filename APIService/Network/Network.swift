//
//  Network.swift
//  APIService
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 4/30/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

import Foundation

protocol Network {
    static func executeRequest() -> String
}
