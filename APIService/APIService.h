//
//  APIService.h
//  APIService
//
//  Created by Dharmarajan Krishnamoorthy (VJ) on 4/30/20.
//  Copyright © 2020 Excentus Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for APIService.
FOUNDATION_EXPORT double APIServiceVersionNumber;

//! Project version string for APIService.
FOUNDATION_EXPORT const unsigned char APIServiceVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <APIService/PublicHeader.h>


